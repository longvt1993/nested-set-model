<?php
namespace NESTED;

use NESTED\Nested_set;

class TestNested {
	public function __construct() {
		add_action( 'save_post_st_test', [ $this, 'create_table_test' ], 10, 3 );
		add_action( 'after_setup_theme', [ &$this, '_check_table_location_nested' ], 50 );
		add_action( 'delete_post', [ $this, 'st_delete_location_nested' ], 50 );
		add_action( 'init', [ $this, 'st_get_child_node' ] );
	}

	public function st_get_child_node() {
		global $wpdb;
		$ns = new Nested_set();
		$ns->setControlParams( $wpdb->nested );
		$node = $ns->getAllChildNode( 2, 9 );
	}

	public function create_table_test( $post_id, $post, $update ) {
		if ( get_post_type( $post_id ) == 'st_test' && get_post_field( 'post_status', $post_id ) != 'auto-draft' ) {
			global $wpdb;
			$ns = new Nested_set();
			$ns->setControlParams( $wpdb->nested );
			$location_id = (int) $post_id;
			$name        = get_the_title( $post_id );
			$parent_id   = (int) get_post_field( 'post_parent', $post_id );
			$node        = $ns->getNodeWhere( 'nested_id = ' . $post_id );
			if ( ! empty( $node ) ) { // Update
				$target = $ns->getNodeWhere( 'nested_id = ' . $parent_id );
				$ns->setNodeAsLastChild( $node, $target );
				$new_node = $ns->getNodeWhere( 'nested_id = ' . $post_id );
				$string   = $this->getFullName( $new_node, esc_html( $post->post_title ) );
				$this->updateExtrafield( $location_id, [
					'name' => $name,
				] );
			} elseif ( $parent_id == 0 ) { // Add Node dưới trực tiếp node root
				$new_node = $this->setFirstChild( $parent_id, $location_id, $name );
				$string   = $this->getFullName( $new_node, esc_html( $post->post_title ) );
				$this->updateExtrafield( (int) $new_node['nested_id'], [
					'name' => $string,
				] );
			} else { // Add các note con
				$new_node = $this->setChild( $parent_id, $location_id, $name );
				$string   = $this->getFullName( $new_node, esc_html( $post->post_title ) );
				$this->updateExtrafield( (int) $new_node['nested_id'], [
					'name' => $string,
				] );
			}
		}
	}

	public function setChild( $parent_id = 0, $nested_id = 0, $name = '' ) {
		global $wpdb;
		$ns = new Nested_set();
		$ns->setControlParams( $wpdb->nested );
		$node_parent = $ns->getNodeWhere( 'nested_id = ' . $parent_id );
		if ( empty( $node_parent ) ) {
			$this->setFirstChild( $parent_id, $nested_id, $name );
		} else {
			$node     = [
				'id'        => (int) $node_parent['id'],
				'parent_id' => (int) $node_parent['parent_id'],
				'left_key'  => (int) $node_parent['left_key'],
				'right_key' => (int) $node_parent['right_key'],
			];
			$extra    = [
				'name'      => $name,
				'nested_id' => $nested_id,
			];
			$new_node = $ns->appendNewChild( $node, $extra );

			return $new_node;
		}
	}

	public function setFirstChild( $parent_id = 0, $nested_id = 0, $name = '' ) {
		global $wpdb;
		$ns = new Nested_set();
		$ns->setControlParams( $wpdb->nested );
		$node_root = $ns->getRootNodes();
		$node      = [
			'id'        => (int) $node_root[0]['id'],
			'parent_id' => (int) $node_root[0]['parent_id'],
			'left_key'  => (int) $node_root[0]['left_key'],
			'right_key' => (int) $node_root[0]['right_key'],
		];
		$extra     = [
			'nested_id' => $nested_id,
			'name'      => $name,
		];
		$new_node  = $ns->appendNewChild( $node, $extra );

		return $new_node;
	}

	public function getFullName( $node, $new_name = '' ) {
		global $wpdb;
		$ns = new Nested_set();
		$ns->setControlParams( $wpdb->nested );
		if ( empty( $node ) ) {
			return '';
		}
		$string = isset( $node['name'] ) ? $node['name'] : '';
		if ( ! empty( $new_name ) ) {
			$string = $new_name;
		}

		$tree = $ns->getNodesWhere( 'left_key < ' . (int) $node['left_key'] . ' AND right_key > ' . (int) $node['right_key'] . ' AND nested_id <> 0', 'left_key DESC' );
		if ( ! empty( $tree ) ) {
			foreach ( $tree as $key => $item ) {
				$string .= ', ' . $item['name'];
			}
		}
		// $string .= $this->getZipCodeHtml( (int) $node['location_id'] );

		return $string;
	}

	public function updateExtrafield( $location_id, $extrafields = [] ) {
		global $wpdb;
		$table = $wpdb->nested;
		$data  = $extrafields;
		$where = [
			'nested' => $location_id,
		];
		$wpdb->update( $table, $data, $where );
	}

	public function _check_table_location_nested() {
		global $wpdb;
		$sql     = 'SELECT id FROM ' . $wpdb->nested . ' LIMIT 1';
		$num_row = (int) $wpdb->get_var( $sql );
		if ( $num_row <= 0 ) {
			$ns = new Nested_set();
			$ns->setControlParams( $wpdb->nested );
			$ns->insertNewTree( [
				'nested_id' => 0,
				'parent_id' => 0,
				'name'      => 'root',
			] );
		}
	}

	public function st_delete_location_nested( $post_id ) {
		if ( get_post_type( $post_id ) == 'st_test' ) {
			global $wpdb;
			$ns = new Nested_set();
			$ns->setControlParams( $wpdb->nested );
			$parent      = (int) get_post_field( 'post_parent', $post_id );
			$node_parent = $ns->getNodeWhere( 'nested_id = ' . $parent );
			$node        = $ns->getNodeWhere( 'nested_id = ' . $post_id );
			if ( ! empty( $node ) ) {
				$node_child = $ns->getFirstChild( $node );
				if ( ! empty( $node_child ) ) {
					$ns->setNodeAsLastChild( $node_child, $node_parent );
				}
				$node = $ns->getNodeWhere( 'nested_id = ' . $post_id );
				if ( ! empty( $node ) ) {
					$ns->deleteNode( $node );
				}
			}
		}
	}

	private function get_title( int $post_id = 0 ) {
		return get_the_title( $post_id );
	}
}
